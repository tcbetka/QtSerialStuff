#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QSerialPort>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_btnClearInput_clicked();
    void on_btnSendCommand_clicked();
    void on_btnCapturePort_clicked();
    void on_btnReleasePort_clicked();
    void on_btnClearOutput_clicked();
    void read_data();

private:
    Ui::MainWindow *ui;
    QSerialPort *serial;
    QList<QSerialPortInfo> systemPorts;
    const unsigned int BUF_SIZE;
    void get_ports();
    void setup_form();
    void clear_input_field();
    bool configure_port(const QString port, const int baud, const int dataBits);
    void disable_controls();
    void enable_controls();
    void parse_string(const QString input );
};

#endif // MAINWINDOW_H
