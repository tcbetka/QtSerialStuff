#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QtSerialPort>
#include <QSerialPortInfo>
#include <QTextEdit>
#include <QMessageBox>
#include <QDebug>
#include <QTextStream>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    BUF_SIZE(80)
{
    ui->setupUi(this);
    serial = new QSerialPort(this);
    systemPorts = QList<QSerialPortInfo>();

    setup_form();
} // end ctor

MainWindow::~MainWindow()
{
    delete ui;
} // end dtor

void MainWindow::setup_form()
{
    // Enumerate the available serial ports
    get_ports();

    // Fill the baud rate combobox
    QList<QString> baudRates = QList<QString>() << "4800" << "9600"
                          << "19200" << "38400"<< "57600" << "115200";
    QStringList baudRateList(baudRates);
    ui->cboBaudRate->addItems(baudRateList);
    ui->cboBaudRate->setCurrentIndex(1);

    // Fill the data bit combobox
    QList<QString> dataBits = QList<QString>() << "5" << "6" << "7" << "8";
    QStringList dataBitsList(dataBits);
    ui->cboDataBits->addItems(dataBitsList);
    ui->cboDataBits->setCurrentIndex(3);

    // Configure the radio buttons
    ui->radStopBit->setChecked(true);
    ui->radNoParity->setChecked(true);
    ui->radNoFlow->setChecked(true);
    ui->btnReleasePort->setEnabled(false);
    ui->txtSerialInput->setEnabled(false);

//TODO: Separate the LCDNumbers into their own palettes
    // Set the LCD colors
    ui->lcdTime->setAutoFillBackground(true);
    ui->lcdEkg->setAutoFillBackground(true);
    ui->lcdWaveA->setAutoFillBackground(true);
    ui->lcdWaveB->setAutoFillBackground(true);
    ui->lcdMbp->setAutoFillBackground(true);
    palLcds = ui->lcdTime->palette();
    palLcds.setColor(QPalette::Normal, QPalette::WindowText, Qt::green);
    palLcds.setColor(QPalette::Normal, QPalette::Window, Qt::black);
    ui->lcdTime->setPalette( palLcds );
    ui->lcdEkg->setPalette( palLcds );
    ui->lcdWaveA->setPalette( palLcds );
    ui->lcdWaveB->setPalette( palLcds );
    ui->lcdMbp->setPalette( palLcds );
} // end setup_form()

void MainWindow::get_ports()
{
    systemPorts = QSerialPortInfo::availablePorts();
    ui->cboSerialPort->addItem("None found");

    foreach( QSerialPortInfo port, systemPorts )
    {
        /*
        // Only add the port to the combo box list if the name contains "USB"
        if( port.portName().contains("USB") || port.portName().contains("ACM") )
        {
            // Can also use port.systemLocation() to give full path name
            ui->cboSerialPort->addItem( port.portName() );
        }
        */
        ui->cboSerialPort->addItem( port.portName() );
    }

    // We don't want to default to the "None found" option unless there are no ports available
    if( ui->cboSerialPort->count() > 1 )
    {
        ui->cboSerialPort->setCurrentIndex(1);
        ui->btnCapturePort->setEnabled(true);
    }
} // end get_ports()

void MainWindow::on_btnCapturePort_clicked()
{
    // Port
    QString port = ui->cboSerialPort->currentText();

    // Baud rate
    QString baud = ui->cboBaudRate->currentText();
    int baudRate = baud.toInt();

    // Data bits
    QString bits = ui->cboDataBits->currentText();
    int dataBits = bits.toInt();

    bool isPortReady = configure_port(port, baudRate, dataBits);
    QMessageBox msgBox;
    if( isPortReady )
    {
        {
            msgBox.setText("Serial port ready!");
            msgBox.exec();
            disable_controls();
            clear_input_field();
            ui->btnReleasePort->setEnabled(true);
            ui->txtSerialInput->setEnabled(true);
            ui->txtSerialInput->setFocus();

            // Create a signal/slot relationship to start reading data from port
            connect( serial, SIGNAL(readyRead()), this, SLOT(read_data()) );

            serial->flush();
        }
    }
    else
    {
        msgBox.setText("Error configuring port");
        msgBox.exec();
    }
} // end on_btnCapturePort_clicked()

void MainWindow::on_btnReleasePort_clicked()
{
    serial->flush();
    serial->close();
    QMessageBox msgBox;
    msgBox.setText("Serial port closed");
    msgBox.exec();
    enable_controls();
    ui->btnReleasePort->setEnabled(false);
    ui->txtSerialInput->setEnabled(false);
}

void MainWindow::disable_controls()
{
    ui->btnCapturePort->setEnabled(false);
    ui->cboSerialPort->setEnabled(false);
    ui->cboBaudRate->setEnabled(false);
    ui->cboDataBits->setEnabled(false);
}

void MainWindow::enable_controls()
{
    ui->btnCapturePort->setEnabled(true);
    ui->cboSerialPort->setEnabled(true);
    ui->cboBaudRate->setEnabled(true);
    ui->cboDataBits->setEnabled(true);
}

void MainWindow::read_data()
{
    // NOTE: readLine() uses a char[], whereas readAll() uses a QByteArray
    char inData[BUF_SIZE];
    qint64 count = serial->readLine(inData, BUF_SIZE);
    if( count > 0 )
    {
        qDebug() << count << " bytes were read.";

        QString output = inData;
        parse_string( output );

//        ui->txtSerialOutput->moveCursor( QTextCursor::End );
//        ui->txtSerialOutput->insertPlainText( output );
//        ui->txtSerialOutput->moveCursor( QTextCursor::End );
    }
    else if( count == 0 )
    {
        qDebug() << "No data read";
    }
} // end read_data()

void MainWindow::parse_string(const QString input )
{
    int finalVals[5] = { 0 };
    QString trimVals[5];

    // Split the string into integer tokens
    QStringList list = input.split(",");

    if( list.length() != 5 ) {
        return;
    }

    // Trim the list elements
    for(int i = 0; i < list.length(); ++i )
    {
        trimVals[i] = list.at(i);
        trimVals[i] = trimVals[i].trimmed();
    }

    finalVals[0] = trimVals[0].toInt();
    finalVals[1] = trimVals[1].toInt();
    finalVals[2] = trimVals[2].toInt();
    finalVals[3] = trimVals[3].toInt();
    finalVals[4] = trimVals[4].toInt();


    ui->txtSerialOutput->append( "Time: " + QString::number(finalVals[0]) );
    ui->txtSerialOutput->append( "EKG: " + QString::number(finalVals[1]) );
    ui->txtSerialOutput->append( "WaveA: " + QString::number(finalVals[2]) );
    ui->txtSerialOutput->append( "WaveB: " + QString::number(finalVals[3]) );
    ui->txtSerialOutput->append( "MBP: " + QString::number(finalVals[4]) );
}

bool MainWindow::configure_port(const QString port, const int baudRate, const int dataBits)
{
    bool flag = false;

    // Set desired port interface
    foreach( const QSerialPortInfo p, systemPorts )
    {
        if( p.portName() == port )
            serial->setPort(p);
    }

    // Note: QSerialPort inherits from QIODevice
    if( serial->open(QIODevice::ReadWrite) )
    {
        // Set desired baud rate
        switch( baudRate )
        {
            case 4800:
                flag = serial->setBaudRate(QSerialPort::Baud4800);
                break;
            case 9600:
                flag = serial->setBaudRate(QSerialPort::Baud9600);
                break;
            case 19200:
                flag = serial->setBaudRate(QSerialPort::Baud19200);
                break;
            case 38400:
                flag = serial->setBaudRate(QSerialPort::Baud38400);
                break;
            case 57600:
                flag = serial->setBaudRate(QSerialPort::Baud57600);
                break;
            case 115200:
                flag = serial->setBaudRate(QSerialPort::Baud115200);
                break;
        } // end switch baud rate

        // Set number of data bits
        switch( dataBits )
        {
            case 5:
                flag = serial->setDataBits(QSerialPort::Data5);
                break;
            case 6:
                flag = serial->setDataBits(QSerialPort::Data6);
                break;
            case 7:
                flag = serial->setDataBits(QSerialPort::Data7);
                break;
            case 8:
                flag = serial->setDataBits(QSerialPort::Data8);
                break;
        } // end switch data bits

        // Set stop bit
        if( ui->radStopBit->isChecked() )
            flag = serial->setStopBits(QSerialPort::OneStop);

        // Set parity
        if( ui->radNoParity->isChecked() )
            flag = serial->setParity(QSerialPort::NoParity);
        else if( ui->radEvenParity->isChecked() )
            flag = serial->setParity(QSerialPort::EvenParity);
        else
            flag = serial->setParity(QSerialPort::OddParity);

        // Set flow control
        if( ui->radNoFlow->isChecked() )
            flag = serial->setFlowControl(QSerialPort::NoFlowControl);
        else if ( ui->radHwFlow->isChecked() )
            flag = serial->setFlowControl(QSerialPort::HardwareControl);
        else
            flag = serial->setFlowControl(QSerialPort::SoftwareControl);
    }

    // The only way this is still 'true' is if all the above config steps were successful
    return flag;
} // end configure_port()

void MainWindow::clear_input_field()
{
    ui->txtSerialInput->clear();
    ui->txtSerialInput->setFocus();
}

void MainWindow::on_btnClearInput_clicked()
{
    clear_input_field();
}

//TODO: Test me!
void MainWindow::on_btnSendCommand_clicked()
{
    // Only transmit if the serial connection is open
    if( !(serial->isOpen()) )
    {
        QMessageBox msgBox;
        msgBox.setText("Error: Serial connection is not open");
        msgBox.exec();
        return;
    }

    // Grab the user input text and convert it to a const char*
    QString input = ui->txtSerialInput->text();

    // Add support to tack on a newline character, in case we need this later
    if( ui->chkAddCrNl->isChecked() )
    {
        input += '\n';
    }

    // Send the data over the serial port
    const char* data = input.toStdString().c_str();
    serial->write(data);

    // Echo to the output section if desired
    if( ui->chkEchoInput->isChecked() )
    {
        ui->txtSerialOutput->append(input);
        ui->txtSerialInput->setFocus();
    }

    clear_input_field();

} // end on_btnSendCommand_clicked()

void MainWindow::on_btnClearOutput_clicked()
{
    ui->txtSerialOutput->clear();
    ui->txtSerialInput->setFocus();
}

